// Fill out your copyright notice in the Description page of Project Settings.

#include "Match3LineDrawer.h"
#include "Match3PlayerController.h"
#include "Match3LineDrawerGameMode.h"

void AMatch3PlayerController::BeginPlay()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
}

void AMatch3PlayerController::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	//Wenn die Maustaste gedr�ckt ist
	if (bclicking)
	{
		//Linetraces ausf�hren
		FHitResult hit;
		GetHitResultUnderCursorByChannel(UEngineTypes::ConvertToTraceType(ECC_Visibility), false, hit);
		if (hit.bBlockingHit)
		{
			AGameblock* block = Cast<AGameblock>(hit.GetActor());
			//Wenn noch kein Block selektiert ist
			if (selected.Num() <= 0)
			{
				//Block dem Array hinzuf�gen und Highlighten
				selected.Add(block);
				block->Highlight();
			}
			else
			{
				//Wenn der zuletzt hinzugef�gte Block und der aktuelle die gleiche Farbe haben
				if (block->GetBlockColor() == selected.Last()->GetBlockColor())
				{
					//Wenn es nicht der gleiche Block ist
					if (block != selected.Last())
					{
						//Wenn erst ein block vohanden ist oder es mehrere sind und es nicht der vorletzte im array ist
						if (selected.Num() < 2 || selected.Num() >= 2 && block != selected[selected.Num() - 2])
						{
							selected.Add(block);
							block->Highlight();
						}
						else
						{
							AGameblock* Rblock = selected.Last();
							Rblock->RemoveHighlight();
							Rblock->SetSelected(false);
							
							selected.Remove(Rblock);
						}
					}

				}
				else
				{
					ResetState();
				}
			}

		}
		else
		{
			ResetState();
		}
	}
}


void AMatch3PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction("MouseClick", IE_Pressed, this, &AMatch3PlayerController::OnMouseDown);
	InputComponent->BindAction("MouseClick", IE_Released, this, &AMatch3PlayerController::OnMouseReleased);
}


void AMatch3PlayerController::OnMouseDown()
{
	bclicking = true;
	nrTurnes++;
}

void AMatch3PlayerController::OnMouseReleased()
{
	bool bpointsCollected = false;
	for (int i = 0; i < selected.Num(); i++)
	{
		//Wenn die punkte noch nicht hochaddiert wurden und die mindestanzahl an bl�cken erreicht wurde
		if (!bpointsCollected && selected.Num() >= 3)
		{
			//Punkte f�r alle Bl�cke aufaddieren
			for (int i = 0; i < selected.Num(); i++)
			{
				score += selected[i]->GetPoints();
			}
			bpointsCollected = true;

			//Methode zum Ver�ndern des Feldes aufrufen
			UWorld* world = GetWorld();
			if (world)
			{
				AMatch3LineDrawerGameMode* gameMode = Cast<AMatch3LineDrawerGameMode>(world->GetAuthGameMode());
				if (gameMode)
				{
					gameMode->MoveBlocks();
				}
			}
		}
		//De-Highlighten

		selected[i]->RemoveHighlight();

	}

	bclicking = false;
	selected.Empty();
}


void AMatch3PlayerController::ResetState()
{
	for (int i = 0; i < selected.Num(); i++)
	{
		selected[i]->RemoveHighlight();

	}
	selected.Empty();
	bclicking = false;
}