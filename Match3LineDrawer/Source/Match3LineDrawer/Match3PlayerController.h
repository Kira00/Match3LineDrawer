 // Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "Gameblock.h"
#include "Match3PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MATCH3LINEDRAWER_API AMatch3PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	void AddScore(int value) { score += value; }

	bool IsClicking() { return bclicking; }

	void ResetScore() { score = 0; }
	int ReturnScore() { return score; }
private:

	void BeginPlay();

	void SetupInputComponent() override;
	
	void Tick(float deltaTime);

	void OnMouseDown();
	void OnMouseReleased();

	/**
	*Methode zum zurücksetzen von FeldHighlighting und Mauszustand
	*/
	void ResetState();

	int score = 0;

	bool bclicking;

	/**
	*Array mit den selektierten Blöcken
	*/
	TArray<AGameblock*> selected;

	int nrTurnes = 0;
};
