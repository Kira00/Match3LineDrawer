// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Gameblock.generated.h"

/**
*Enum zur logischen Bestimmung der Farbe
*/
UENUM()
enum class EColor :uint8
{
	CE_Blue,
	CE_Red,
	CE_Green,
	CE_Yellow,
	NUM_Colors
};

UCLASS()
class MATCH3LINEDRAWER_API AGameblock : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameblock();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	EColor GetBlockColor() { return blockColor; }
	
	float GetWidth() { return width; }
	float GetHeight(){ return height; }

	/**
	*Methode zum Highlighten des Blocks
	*/
	void Highlight();
	
	/**
	*Methode zum De-Highlighten des Blocks
	*/
	void RemoveHighlight();

	void SetSelected(bool value) { bselected = value; }
	bool IsSelected() { return bselected; }

	int GetPoints() { return points; }

	/**
	*Methode zum erstellen eines neuen Blocks(zuf�llige neue Farbe zuweisen)
	*/
	void CreateNewBlock();

	/**
	*Material entsprechend dem Enum einf�rben
	*/
	void ChangeColor(EColor newColor);

protected:
	UStaticMeshComponent* mesh;
	
	EColor blockColor;

	//UPROPERTY(EditEditorOnly, Category = Scale)
	float scale = 0.5;
	
	float width = 346*scale;
	float height = 200 * scale;
	
	int points = 50;

	void SetColor();


private:

	UMaterialInstance* materialInstance;
	
	bool bselected;

};
