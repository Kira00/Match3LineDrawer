// Fill out your copyright notice in the Description page of Project Settings.

#include "Match3LineDrawer.h"
#include "Match3PlayerController.h"
#include "Match3LineDrawerGameMode.h"
#include "PlayerPawn.h"



AMatch3LineDrawerGameMode::AMatch3LineDrawerGameMode() :Super()
{
	//PlayerContrller und Pawn setzen

	PlayerControllerClass = AMatch3PlayerController::StaticClass();
	DefaultPawnClass = APlayerPawn::StaticClass();
}

void AMatch3LineDrawerGameMode::MoveBlocks()
{
	//variable zur �berpr�fung ob sich am Layout des feldes etwas ver�ndert hat
	bool bfieldChanged = false;

	do
	{		
		bfieldChanged = false;

		//Spalten und Reihen des Feldes durchgehen
		for (int i = 0; i < FIELD_HEIGHT; i++)
		{
			for (int j = 0; j < FIELD_WIDTH; j++)
			{
				//wenn das Feld selektiert ist
				if (gameBlocks[i][j]->IsSelected())
				{
					//�nderungsflagge setzen
					bfieldChanged = true;
					//handelt es sich um die Spitze
					if (i + 1 >= FIELD_HEIGHT)
					{
						//neuen block generiern und deselektieren
						gameBlocks[i][j]->CreateNewBlock();
						gameBlocks[i][j]->SetSelected(false);

					}
					else
					{
						//alle felder �ber dem selektierten Feld durchgehen
						for (int k = i; k < FIELD_HEIGHT - 1; k++)
						{
							//untersuchtes Feld zu dar�ber liegenden Feld umf�rben
							gameBlocks[k][j]->ChangeColor(gameBlocks[k + 1][j]->GetBlockColor());
							//ist das dar�ber liegende Feld nicht selektiert
							if (!gameBlocks[k + 1][j]->IsSelected())
							{
								//feld deselektieren
								gameBlocks[k][j]->SetSelected(false);
							}
						}
						gameBlocks[FIELD_HEIGHT - 1][j]->CreateNewBlock();
						gameBlocks[FIELD_HEIGHT - 1][j]->SetSelected(false);

					}
				}
			}
		}
	} while (bfieldChanged); //Solange sich das Feld ver�ndert


}

void AMatch3LineDrawerGameMode::BeginPlay()
{

	UWorld* world = GetWorld();

	FVector location = FVector(0, 0, 0);
	FRotator rotation = FRotator(0, 0, 90);

	AGameblock* newBlock;

	if (world)
	{
		for (int i = 0; i < FIELD_HEIGHT; i++)
		{
			for (int j = 0; j < FIELD_WIDTH; j++)
			{
				//Neues Feld Spawnen und dem Array hinzuf�gen
				newBlock = world->SpawnActor<AGameblock>(location, rotation);
				gameBlocks[i][j] = newBlock;
				//Spawnposition versetzen
				location.X += newBlock->GetWidth();
				if (j % 2 == 0)
				{
					location.Z += newBlock->GetHeight();
				}
				else
				{
					location.Z -= newBlock->GetHeight();

				}
			}
			//Eine Zeile hoch gehen
			location.Z += newBlock->GetHeight();
			location.X = 0;
		}

		//customStart = world->SpawnActor<APlayerStart>();
	}

	Super::BeginPlay();
}

