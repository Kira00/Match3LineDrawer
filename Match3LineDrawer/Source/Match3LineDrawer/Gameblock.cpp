// Fill out your copyright notice in the Description page of Project Settings.

#include "Match3LineDrawer.h"
#include "Gameblock.h"





// Sets default values
AGameblock::AGameblock()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> staticMesh(TEXT("StaticMesh'/Game/CustomMeshes/Hexagon_StaticMesh.Hexagon_StaticMesh'"));
	static ConstructorHelpers::FObjectFinder<UMaterialInstance> materialInstanceHelper(TEXT("MaterialInstanceConstant'/Game/CustomMaterials/FieldMaterial_Instance.FieldMaterial_Instance'"));
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	if (staticMesh.Object)
	{
		mesh->SetStaticMesh(staticMesh.Object);
		if (materialInstanceHelper.Object)
		{
			materialInstance = materialInstanceHelper.Object;
			mesh->SetMaterial(0,materialInstance);

		}
	}
	
}

// Called when the game starts or when spawned
void AGameblock::BeginPlay()
{
	FVector scale3D = FVector(scale, scale, 1);
	mesh->SetWorldScale3D(scale3D);
	
	blockColor = StaticCast<EColor>(rand() % StaticCast<uint8>(EColor::NUM_Colors));
	SetColor();
	

	Super::BeginPlay();
}

// Called every frame
void AGameblock::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	
}

void AGameblock::Highlight()
{
	UMaterialInstanceDynamic* dynamicMaterialInstance = mesh->CreateAndSetMaterialInstanceDynamic(0);
	dynamicMaterialInstance->SetScalarParameterValue("Multiplyer", 2);
	bselected = true;
}

void AGameblock::RemoveHighlight()
{
	UMaterialInstanceDynamic* dynamicMaterialInstance = mesh->CreateAndSetMaterialInstanceDynamic(0);
	dynamicMaterialInstance->SetScalarParameterValue("Multiplyer", 0);
	
}

void AGameblock::CreateNewBlock()
{
	blockColor = StaticCast<EColor>(rand() % StaticCast<uint8>(EColor::NUM_Colors));
	SetColor();
}

void AGameblock::ChangeColor(EColor newColor)
{
	blockColor = newColor;
	SetColor();
}

void AGameblock::SetColor()
{
	//UMaterialInstance* matInstance = mesh->GetMaterial(0);
	UMaterialInstanceDynamic* dynamicMaterialInstance = mesh->CreateAndSetMaterialInstanceDynamic(0);

	switch (blockColor)
	{
	case EColor::CE_Blue:
		dynamicMaterialInstance->SetVectorParameterValue("Color", FLinearColor::Blue);
		break;
	case EColor::CE_Red:
		dynamicMaterialInstance->SetVectorParameterValue("Color", FLinearColor::Red);

		break;
	case EColor::CE_Green:
		dynamicMaterialInstance->SetVectorParameterValue("Color", FLinearColor::Green);

		break;
	case EColor::CE_Yellow:
		dynamicMaterialInstance->SetVectorParameterValue("Color", FLinearColor::Yellow);

		break;
	default:
		break;
	}
}

