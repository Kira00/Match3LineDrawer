// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "Gameblock.h"
#include "Match3LineDrawerGameMode.generated.h"

#define FIELD_WIDTH  7
#define FIELD_HEIGHT 6

/**
 *
 */
UCLASS()
class MATCH3LINEDRAWER_API AMatch3LineDrawerGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	void BeginPlay();

	AMatch3LineDrawerGameMode();

	/**
	*Bl�cke nach "aufl�sen" einer Gruppe "bewegen"
	*/
	void MoveBlocks();

private:
	AGameblock* gameBlocks[FIELD_HEIGHT][FIELD_WIDTH];



};
